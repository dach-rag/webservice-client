<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:f="http://purl.org/dc/elements/1.1/" version="2.0">
   <xsl:output method="xhtml" indent="yes" omit-xml-declaration="yes" standalone="omit" encoding="utf-8" />
   <xsl:template match="/response/result">
      <div id="librisWrapper" class="restClient-content">
         <div>
            Treffer bei Brockhaus:
            <xsl:value-of select="//result/@numFound" />
         </div>
         <div class="portlet-queryRecordSearchResult">
            <xsl:for-each select="//document">
               <div class="arena-record-container arena-library-record">
                  <div class="arena-record clearfix">
                     <div class="arena-record-left">
                        <div class="arena-record-cover">
                           <div class="arena-book-jacket">
                              <img>
                                 <xsl:attribute name="src">
                                    <xsl:value-of select="thumbnail" />
                                 </xsl:attribute>
                                 <xsl:attribute name="alt">
                                    <xsl:value-of select="title" />
                                 </xsl:attribute>
                              </img>
                           </div>
                        </div>
                     </div>
                     <div class="arena-record-details">
                        <div class="arena-record-title">
                           <span>
                              <xsl:value-of select="title" />
                           </span>
                        </div>
                        <div class="arena-record-description">
                           <span class="arena-field">Zusammenfassung:</span>
                           <span class="arena-value">
                              <xsl:value-of select="summary" />
                           </span>
                        </div>
                     </div>
                     <div class="arena-record-button arena-toggle-record-list" />
                  </div>
               </div>
            </xsl:for-each>
         </div>
         <div>
            <a href="{query}"><xsl:value-of select="query"/>Treffer bei Brockhaus anzeigen</a>
         </div>
      </div>
      <div class="script-libris">
         <script type="text/javascript">
           jQuery(document).ready(function() {
             // hide loading indicator when done
             jQuery('.script-libris').parents('.portlet-boundary').find('img[src$="indicator.gif"]').hide();
           });
         </script>
      </div>
   </xsl:template>
</xsl:stylesheet>